extends Control

onready var pages = $Pages

func _ready():
	pass # sensor orientation... on change switch scene

func hide_all_pages():
	pages.visible = false
	pages.get_node("Journal").visible = false
	pages.get_node("Supplies").visible = false
	pages.get_node("Crops").visible = false
	pages.get_node("Livestock").visible = false
	pages.get_node("Planning").visible = false
	pages.get_node("Property").visible = false
	pages.get_node("Configure").visible = false
	pages.get_node("Help").visible = false

func _on_btn_Journal_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Journal").visible = true

func _on_btn_JournalHome_pressed():
	hide_all_pages()

func _on_btn_Supplies_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Supplies").visible = true

func _on_btn_Crops_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Crops").visible = true

func _on_btn_Livestock_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Livestock").visible = true

func _on_btn_Planning_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Planning").visible = true

func _on_btn_Property_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Property").visible = true

func _on_btn_Configure_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Configure").visible = true

func _on_btn_Help_pressed():
	hide_all_pages()
	pages.visible = true
	pages.get_node("Help").visible = true

func _on_btn_SuppliesHome_pressed():
	hide_all_pages()

func _on_btn_CropsHome_pressed():
	hide_all_pages()

func _on_btn_LivestockHome_pressed():
	hide_all_pages()

func _on_btn_PlanningHome_pressed():
	hide_all_pages()

func _on_btn_PropertyHome_pressed():
	hide_all_pages()

func _on_btn_ConfigureHome_pressed():
	hide_all_pages()

func _on_btn_HelpHome_pressed():
	hide_all_pages()
