extends Panel

onready var MAIN = get_tree().root.get_node("Main")

# There is a bug where the tasks do not refresh, but it does not effect finish_log

func _on_btn_TaskDone_pressed():
	for task in MAIN.test_data["employees"][0]["tasks"]:
		if task["id"] == name:
			task["last_finish"] = Time.get_unix_time_from_system()
			task["last_state"] = "Completed"
			task["finish_log"].append({"finisher": MAIN.test_data["employees"][0]["id"], "state": "Completed", "note": MAIN.get_node("Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer/"+name+"/HBoxContainer/le_Notes").text, "finish": Time.get_unix_time_from_system()})
	MAIN.load_tasks()
#	for task in MAIN.test_data["employees"][0]["tasks"]:
#		print(task["finish_log"].size())

func _on_btn_TaskIgnore_pressed():
	for task in MAIN.test_data["employees"][0]["tasks"]:
		if task["id"] == name:
			task["last_finish"] = Time.get_unix_time_from_system()
			task["last_state"] = "Ignored"
			task["finish_log"].append({"finisher": MAIN.test_data["employees"][0]["id"], "state": "Ignored", "note": MAIN.get_node("Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer/"+name+"/HBoxContainer/le_Notes").text, "finish": Time.get_unix_time_from_system()})
	MAIN.load_tasks()
#	for task in MAIN.test_data["employees"][0]["tasks"]:
#		print(task["finish_log"].size())
