extends Panel

onready var MAIN = get_tree().root.get_node("Main")

func _on_btn_ViewFullNote_pressed():
	print(MAIN.test_data)
	MAIN.get_node("Center/Pages/Journal/tc_Notebook/Previous Notes/ViewFullNote/ScrollContainer/te_Notes").text = MAIN.test_data["employees"][0]["notes"][name]["content"]
	MAIN.get_node("Center/Pages/Journal/tc_Notebook/Previous Notes/ViewFullNote/label_DateTime").text = MAIN.test_data["employees"][0]["notes"][name]["date"]
	MAIN.get_node("Center/Pages/Journal/tc_Notebook/Previous Notes/ViewFullNote").visible = true

func _on_btn_DeleteNote_pressed():
	MAIN.test_data["employees"][0]["notes"].erase(name)
	MAIN.save_data()
	queue_free()
