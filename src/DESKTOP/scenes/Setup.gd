extends Control

onready var MAIN = $"../"

var setup_pos = 0

var setup_data = {
	"first_name": "",
	"last_name": "",
	"organization": ""
}

func _on_btn_SetupNext_pressed():
	if setup_pos == 0:
		$NextButton.visible = false
		$label_Directions.visible = false
		$Setup1.visible = true
	elif has_node("Setup1"+str(setup_pos)):
		get_node("Setup"+str(setup_pos)).visible = true
	else:
		$".".visible = false
		var setup_data = {
			"first_name": "",
			"last_name": "",
			"organization": ""
		}
		
		setup_data["organiziation"] = $Setup1/VBoxContainer/vbox_Inputs/organization.text
		setup_data["first_name"] = $Setup1/VBoxContainer/vbox_Inputs/first_name.text
		setup_data["last_name"] = $Setup1/VBoxContainer/vbox_Inputs/last_name.text
		if setup_data["organization"] == "":
			setup_data["organizaiton"] = "Private Farm"
		if setup_data["first_name"] == "":
			setup_data["first_name"] = "Unknown"
		MAIN.test_data = {}
		MAIN.test_data["employees"] = []
		MAIN.test_data["employees"].append({})
		MAIN.test_data["employees"][0]["first_name"] = setup_data["first_name"]
		MAIN.test_data["employees"][0]["last_name"] = setup_data["last_name"]
		MAIN.test_data["employees"][0]["id"] = MAIN.random_fingerprint()
		MAIN.test_data["employees"][0]["role"] = "Manager"
		MAIN.test_data["employees"][0]["notes"] = {}
		MAIN.test_data["employees"][0]["tasks"] = []
		MAIN.test_data["organization"] = setup_data["organization"]
		
		MAIN.test_data["supplies"] = []
		MAIN.test_data["crops"] = []
		MAIN.test_data["livestock"] = []
		MAIN.test_data["supplies"] = []
		
		MAIN.save_data()
		MAIN.load_data()
		MAIN.run_tutorial()
	setup_pos += 1
