extends Control

onready var pages = $Center/Pages

var test_data = null

# using [0] for employee until have more time to work on id system

func _ready():
	load_data()

func load_data():
	var file = File.new()
	if file.file_exists("user://data.json"):
		if file.open("user://data.json", File.READ) == OK:
			var json_string = file.get_as_text()
			var data = parse_json(json_string)
			test_data = data
			$TopPanel/HBoxContainer/label_Name.text = test_data["employees"][0]["first_name"]+" "+test_data["employees"][0]["last_name"]
			$TopPanel/label_Role.text = test_data["employees"][0]["role"]
			# Load Tasks
			var sample_task = {
			"id": random_fingerprint(),
			"employee": "",
			"repetition": 86400, 
			"description": "feed chickens", 
			"last_finish": Time.get_unix_time_from_system()-86400,
			"last_state": "Completed",
			"finish_log": [{"finisher": test_data["employees"][0]["id"], "state": "Completed", "note": "nothing unusual except they were making loud noises as always", "finish": Time.get_unix_time_from_system()-86400}]
			}
			var sample_task2 = {
			"id": random_fingerprint(),
			"employee": "",
			"repetition": 604800, 
			"description": "water trees", 
			"last_finish": Time.get_unix_time_from_system()-604800,
			"last_state": "Completed",
			"finish_log": []
			}
			test_data["employees"][0]["tasks"].append(sample_task)
			test_data["employees"][0]["tasks"].append(sample_task2)
			load_tasks()
			# Load Notes
			if test_data["employees"][0]["notes"].has(Time.get_date_string_from_system(true)):
				$Center/Pages/Journal/tc_Notebook/Today/te_Notes.text = test_data["employees"][0]["notes"][Time.get_date_string_from_system(true)]["content"]
				$Center/Pages/Journal/tc_Notebook/Today/label_DateTime.text = test_data["employees"][0]["notes"][Time.get_date_string_from_system(true)]["date"]
			for note in test_data["employees"][0]["notes"]:
				if note != Time.get_date_string_from_system(true):
					var new_note = preload("res://scenes/Instanced/Note.tscn").instance()
					new_note.get_node("label_Date").text =  test_data["employees"][0]["notes"][note]["date"] #or note
					new_note.get_node("label_Preview").text = test_data["employees"][0]["notes"][note]["content"]
					new_note.name = test_data["employees"][0]["notes"][note]["date"]
					$"Center/Pages/Journal/tc_Notebook/Previous Notes/ScrollContainer/GridContainer".add_child(new_note)
			# Load Supplies
			# Load Crops
			# Load Livestock
			# Load Map
			# Load Settings
			file.close()
		else:
			print("Failed to open saved data.")
	else:
		print("No data exists!")
		hide_all_pages()
		$Setup.visible = true

func load_tasks():
	for child in $"Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer".get_children():
		if child.name != "CreateNew":
			child.queue_free()
	var latest_task
	for task in test_data["employees"][0]["tasks"]:
		if Time.get_unix_time_from_system()-task["last_finish"] > task["repetition"]:
			var new_task = preload("res://scenes/Instanced/Task.tscn").instance()
			$"Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer".add_child(new_task)
			new_task.get_node("label_Title").text = task["description"]
			new_task.get_node("HBoxContainer/VBoxContainer/TaskDone").visible = true
			new_task.get_node("HBoxContainer/VBoxContainer/TaskIgnore").visible = true
			new_task.get_node("HBoxContainer/VBoxContainer/TaskRedo").visible = false
			new_task.get_node("HBoxContainer/VBoxContainer/TaskStateCompleted").visible = false
			new_task.get_node("HBoxContainer/VBoxContainer/TaskStateIgnored").visible = false
			new_task.name = task["id"]
			latest_task = new_task
	for task in test_data["employees"][0]["tasks"]:
		if Time.get_unix_time_from_system()-task["last_finish"] < task["repetition"]:
			var new_task = preload("res://scenes/Instanced/Task.tscn").instance()
			$"Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer".add_child(new_task)
			new_task.get_node("label_Title").text = task["description"]
			new_task.name = task["id"]
			new_task.get_node("HBoxContainer/VBoxContainer/TaskRedo").visible = true
			new_task.get_node("HBoxContainer/VBoxContainer/TaskState"+task["last_state"]).visible = true
			new_task.get_node("HBoxContainer/VBoxContainer/TaskDone").visible = false
			new_task.get_node("HBoxContainer/VBoxContainer/TaskIgnore").visible = false
			latest_task = new_task
	$"Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer/".move_child($"Center/Pages/Journal/tc_Tasks/My Tasks/ScrollContainer/GridContainer/CreateNew", latest_task.get_index())

func save_data():
	var json_string = to_json(test_data)
	var file = File.new()
	file.open("user://data.json", file.WRITE)
	file.store_string(json_string)
	file.close()
	print(json_string)

func random_fingerprint():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	# Generate a random fingerprint by concatenating several random integers
	var fingerprint = ""
	for i in range(5):  # Adjust the range to change the length of the fingerprint
		var random_int = rng.randi() % 100000  # Generate a random integer between 0 and 99999
		fingerprint += str(random_int)
	return fingerprint

func run_tutorial():
	$Tutorial.visible = true

func hide_all_pages():
	pages.get_node("Journal").visible = false
	pages.get_node("Supplies").visible = false
	pages.get_node("Crops").visible = false
	pages.get_node("Livestock").visible = false
	pages.get_node("Planning").visible = false
	pages.get_node("Property").visible = false
	pages.get_node("Configure").visible = false
	pages.get_node("Help").visible = false

func _on_btn_Home_pressed():
	hide_all_pages()

func _on_btn_Journal_pressed():
	hide_all_pages()
	load_tasks()
	pages.get_node("Journal").visible = true

func _on_btn_Supplies_pressed():
	hide_all_pages()
	pages.get_node("Supplies").visible = true

func _on_btn_Crops_pressed():
	hide_all_pages()
	pages.get_node("Crops").visible = true

func _on_btn_Livestock_pressed():
	hide_all_pages()
	pages.get_node("Livestock").visible = true

func _on_btn_Planning_pressed():
	hide_all_pages()
	pages.get_node("Planning").visible = true

func _on_btn_Property_pressed():
	hide_all_pages()
	pages.get_node("Property").visible = true

func _on_btn_Configure_pressed():
	hide_all_pages()
	pages.get_node("Configure").visible = true

func _on_btn_Help_pressed():
	hide_all_pages()
	pages.get_node("Help").visible = true
