extends Control

var tutorial_pos = 0

func _on_btn_TutorialSkip_pressed():
	$Tutorial10.visible = true
	$label_Directions.visible = false
	$NextButton.visible = false
	$SkipTutorialButton.visible = false
	tutorial_pos=10

func _on_btn_TutorialNext_pressed():
	if tutorial_pos == 0:
		$label_Directions.visible = false
		$NextButton.visible = false
		$SkipTutorialButton.visible = false
		$"../Center/Pages/Journal".visible = true
		$Tutorial0.visible = false
		$Tutorial1.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 1:
		$"../Center/Pages/Journal".visible = false
		$"../Center/Pages/Supplies".visible = true
		$Tutorial1.visible = false
		$Tutorial2.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 2:
		$"../Center/Pages/Supplies".visible = false
		$"../Center/Pages/Crops".visible = true
		$Tutorial2.visible = false
		$Tutorial3.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 3:
		$"../Center/Pages/Crops".visible = false
		$"../Center/Pages/Livestock".visible = true
		$Tutorial3.visible = false
		$Tutorial4.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 4:
		$"../Center/Pages/Livestock".visible = false
		$"../Center/Pages/Planning".visible = true
		$Tutorial4.visible = false
		$Tutorial5.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 5:
		$"../Center/Pages/Planning".visible = false
		$"../Center/Pages/Property".visible = true
		$Tutorial5.visible = false
		$Tutorial6.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 6:
		$"../Center/Pages/Property".visible = false
		$"../Center/Pages/Configure".visible = true
		$Tutorial6.visible = false
		$Tutorial7.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 7:
		$"../Center/Pages/Configure".visible = false
		$"../Center/Pages/Help".visible = true
		$Tutorial7.visible = false
		$Tutorial8.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 8:
		$"../Center/Pages/Help".visible = false
		$Tutorial8.visible = false
		$Tutorial9.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 9:
		$Tutorial9.visible = false
		$Tutorial10.visible = true
		tutorial_pos+=1
	elif tutorial_pos == 10:
		$Tutorial10.visible = false
		$Tutorial0.visible = true
		visible = false
		$"../Center/Pages/Journal".visible = true
		tutorial_pos=0
