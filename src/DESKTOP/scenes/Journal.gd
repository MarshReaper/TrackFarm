extends Control

onready var MAIN = $"../../.."

func _ready():
	$tc_Notebook/Today/label_DateTime.text = Time.get_date_string_from_system(true)

func _on_btn_SaveNote_pressed():
	MAIN.test_data["employees"][0]["notes"][Time.get_date_string_from_system(true)] = {}
	MAIN.test_data["employees"][0]["notes"][Time.get_date_string_from_system(true)]["content"] = $tc_Notebook/Today/te_Notes.text
	MAIN.test_data["employees"][0]["notes"][Time.get_date_string_from_system(true)]["date"] = Time.get_date_string_from_system(true)
	MAIN.save_data()

func _on_btn_GoBackNote_pressed():
	$"tc_Notebook/Previous Notes/ViewFullNote/ScrollContainer/te_Notes".text = ""
	$"tc_Notebook/Previous Notes/ViewFullNote".visible = false

func _on_btn_DeleteNote_pressed():
	MAIN.test_data["employees"][0]["notes"].erase($"tc_Notebook/Previous Notes/ViewFullNote/label_DateTime".text)
	MAIN.save_data()
	$"tc_Notebook/Previous Notes/ViewFullNote".visible = false
	$"tc_Notebook/Previous Notes/ScrollContainer/GridContainer".get_node($"tc_Notebook/Previous Notes/ViewFullNote/label_DateTime".text).queue_free()

func _on_btn_ClearNote_pressed():
	$tc_Notebook/Today/te_Notes.text = ""

func _on_btn_TaskCreate_pressed():
	$"tc_Tasks/My Tasks/CreateTask".visible = true

func _on_btn_GoBackTask_pressed():
	$"tc_Tasks/My Tasks/CreateTask".visible = false

func _on_btn_SubmitTask_pressed():
	$"tc_Tasks/My Tasks/CreateTask".visible = false
	var new_task = {
	"id": MAIN.random_fingerprint(),
	"repetition": 0, 
	"description": "", 
	"last_finish": 0,
	"last_state": "Completed",
	"finish_log": []
	}
	new_task["repetition"] = int($"tc_Tasks/My Tasks/CreateTask/ScrollContainer/VBoxContainer/le_TaskRepetition".text)
	new_task["description"] = $"tc_Tasks/My Tasks/CreateTask/ScrollContainer/VBoxContainer/le_TaskDescription".text
	new_task["last_finish"] =  Time.get_unix_time_from_system()-new_task["repetition"]
	MAIN.test_data["employees"][0]["tasks"].append(new_task)
	MAIN.load_tasks()
