# Measurements
### Types of variables that can easily be taken to monitor productivity, efficiency and general progress

#### Produce Harvested (pounds)
- 2lbs of tomatoes
- 3lbs of turnips
- 4lbs of black eyed peas

or ounces or something

#### Resources Used (specific)
- Feed for animals
- Fuel amounts used
- Medicine used

This software may not be meant for calculating dollar costs, but an extra variable can be added I suppose.

#### Soil Testing
- pH (alkaline/acidic)
- Moisture (dry/wet)
- Nutrient (low/high

#### Property and Planning
Measurements of area sizes could be taken to determine drone shots and such.

Events can be linked to gains and losses ... and a reason can be attached to each

Event - Aug 30 2023 - Task, feed chickens - Supply, 4lbs chicken feed - Livestock, chickens group 4
