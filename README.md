# TrackFarm

A simple program used for tracking a small farm and its assets. Record your produce, losses, livestock, supplies and any events that may occur.

## Features
- Journal Events
- Basic Animal Status and Logs
- Daily Checklist (can add syncing to app later)
- Supplies List and Locations
- Farm Map and Geological Data with property lines
- Simple and intuitive but pretty interface
- Growth calculations and projections

### Potential Features
- Weather Forecasts
- Calculated Warnings
- Communicate with networks and devices (post data to this port at this address)
- Device to Device data transfer

This will have portable tools built-in to analyze and document. Downloadable documentation and helpful guides related to agriculture may be available.

Syncing would require a functioning router to connect to the ip address of the computer. Alternatively, a USB connection could be utilized to transfer any data.

Of course, there are already farm management software and services out there. But many of them have many problems.

Some can have extensive fees that are just plain stupid (I love FOSS). Others which are free to use and typically open source are either outdated or extremely outdated. There can be features that are missing or a bad license that was chosen. A farmer may know how to self host but may not have the time. An offline solution would allow progressive development towards fixes to issues that come from being away from good internet or having to cover a large area that may not receive good signal with the current equipment.

To keep it offline by default, a device to device data transfer feature will be implemented.

Some assets may need to be replaced in the future.

### Contact
For any questions or inquiries, please reach out to the [email on my profile](https://codeberg.org/MarshReaper).

If you would like to send a donation after this project is reasonably complete, send an email.
