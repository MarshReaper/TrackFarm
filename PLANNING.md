MAIN MENU

BUTTONS ON LEFT WITH NAMES AND IMAGES
RECENT LOGS, EVENTS AND STATISTICS ON RIGHT
ALSO TASKS AND JOURNAL ETC

BUTTONS HAVE RELATED IMAGE AND TEXT UNDERNEATH

- JOURNAL
- SUPPLIES/INVENTORY
- CROPS
- LIVESTOCK
- PLANNING
- PROPERTY
- CONFIGURE
- HELP

MAIN MENU WILL BE CUSTOMIZABLE IN FUTURE
opening tutorial (replayable) will ask which features to keep or drop

### JOURNAL
write logs, notes based on date
view tasks and farmhand tasks
distribute tasks to people and set priorities
might add messaging
syncing will require connection to computer actively running the program if using mobile device. user must use start server button to allow any syncing via network else use usb on computer and hit sync

### CROPS
Will need to look at other programs and what kind of data would be logged or calculated

### LIVESTOCK
Body count stuff, feed, special care procedures, etc and more

### PLANNING
Easy tools for drawing, mapping and planning out areas. Stamps, zones, roads and so on. Custom colors, tags and names. Precise geolocation tracking for mobile device.

### PROPERTY
Property lines, geography, etc goes hand in hand with planning maybe
Take photo with drone or satellite

### HELP
guides and about the program

### CONFIGURE
settings menu

I will need to research what a farm needs, common daily tasks, what kind of measurements are taken, how they operate efficiently.

I do not have the experience of running a large scale agricultural business, so I cannot say whether such tools would really be more useful than a small notebook. I hope with this project I will further my skills in user interface design, agricultural knowledge and such on.

Maybe this will turn into a glorified task manager app. I don't know. Apparently Excel Sheets are part of a good farmer's tool set. I would need to confirm what kind of data is processed and how it's used.
